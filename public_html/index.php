<?php


/**
 * PHP REST Client
 * https://github.com/tcdent/php-restclient
 * (c) 2013 Travis Dent <tcdent@gmail.com>
 */

class RestClientException extends Exception {}

class RestClient implements Iterator, ArrayAccess {

  public $options;
  public $handle; // cURL resource handle.

  // Populated after execution:
  public $response; // Response body.
  public $headers; // Parsed reponse header object.
  public $info; // Response info object.
  public $error; // Response error string.

  // Populated as-needed.
  public $decoded_response; // Decoded response body.
  private $iterator_positon;

  public function __construct($options=array()){
    $default_options = array(
      'headers' => array(),
      'parameters' => array(),
      'curl_options' => array(),
      'user_agent' => "PHP RestClient/0.1.4",
      'base_url' => NULL,
      'format' => NULL,
      'format_regex' => "/(\w+)\/(\w+)(;[.+])?/",
      'decoders' => array(
        'json' => 'json_decode',
        'php' => 'unserialize'
      ),
      'username' => NULL,
      'password' => NULL
    );

    $this->options = array_merge($default_options, $options);
    if(array_key_exists('decoders', $options))
      $this->options['decoders'] = array_merge(
        $default_options['decoders'], $options['decoders']);
  }

  public function set_option($key, $value){
    $this->options[$key] = $value;
  }

  public function register_decoder($format, $method){
    // Decoder callbacks must adhere to the following pattern:
    //   array my_decoder(string $data)
    $this->options['decoders'][$format] = $method;
  }

  // Iterable methods:
  public function rewind(){
    $this->decode_response();
    return reset($this->decoded_response);
  }

  public function current(){
    return current($this->decoded_response);
  }

  public function key(){
    return key($this->decoded_response);
  }

  public function next(){
    return next($this->decoded_response);
  }

  public function valid(){
    return is_array($this->decoded_response)
    && (key($this->decoded_response) !== NULL);
  }

  // ArrayAccess methods:
  public function offsetExists($key){
    $this->decode_response();
    return is_array($this->decoded_response)?
      isset($this->decoded_response[$key]) : isset($this->decoded_response->{$key});
  }

  public function offsetGet($key){
    $this->decode_response();
    if(!$this->offsetExists($key))
      return NULL;

    return is_array($this->decoded_response)?
      $this->decoded_response[$key] : $this->decoded_response->{$key};
  }

  public function offsetSet($key, $value){
    throw new RestClientException("Decoded response data is immutable.");
  }

  public function offsetUnset($key){
    throw new RestClientException("Decoded response data is immutable.");
  }

  // Request methods:
  public function get($url, $parameters=array(), $headers=array()){
    return $this->execute($url, 'GET', $parameters, $headers);
  }

  public function post($url, $parameters=array(), $headers=array()){
    return $this->execute($url, 'POST', $parameters, $headers);
  }

  public function put($url, $parameters=array(), $headers=array()){
    return $this->execute($url, 'PUT', $parameters, $headers);
  }

  public function delete($url, $parameters=array(), $headers=array()){
    return $this->execute($url, 'DELETE', $parameters, $headers);
  }

  public function execute($url, $method='GET', $parameters=array(), $headers=array()){
    $client = clone $this;
    $client->url = $url;
    $client->handle = curl_init();
    $curlopt = array(
      CURLOPT_HEADER => TRUE,
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_USERAGENT => $client->options['user_agent']
    );

    if($client->options['username'] && $client->options['password'])
      $curlopt[CURLOPT_USERPWD] = sprintf("%s:%s",
        $client->options['username'], $client->options['password']);

    if(count($client->options['headers']) || count($headers)){
      $curlopt[CURLOPT_HTTPHEADER] = array();
      $headers = array_merge($client->options['headers'], $headers);
      foreach($headers as $key => $value){
        $curlopt[CURLOPT_HTTPHEADER][] = sprintf("%s:%s", $key, $value);
      }
    }

    if($client->options['format'])
      $client->url .= '.'.$client->options['format'];

    // Allow passing parameters as a pre-encoded string (or something that
    // allows casting to a string). Parameters passed as strings will not be
    // merged with parameters specified in the default options.
    if(is_array($parameters)){
      $parameters = array_merge($client->options['parameters'], $parameters);
      $parameters_string = $client->format_query($parameters);
    }
    else
      $parameters_string = (string) $parameters;

    if(strtoupper($method) == 'POST'){
      $curlopt[CURLOPT_POST] = TRUE;
      $curlopt[CURLOPT_POSTFIELDS] = $parameters_string;
    }
    elseif(strtoupper($method) != 'GET'){
      $curlopt[CURLOPT_CUSTOMREQUEST] = strtoupper($method);
      $curlopt[CURLOPT_POSTFIELDS] = $parameters_string;
    }
    elseif($parameters_string){
      $client->url .= strpos($client->url, '?')? '&' : '?';
      $client->url .= $parameters_string;
    }

    if($client->options['base_url']){
      if($client->url[0] != '/' && substr($client->options['base_url'], -1) != '/')
        $client->url = '/' . $client->url;
      $client->url = $client->options['base_url'] . $client->url;
    }
    $curlopt[CURLOPT_URL] = $client->url;

    if($client->options['curl_options']){
      // array_merge would reset our numeric keys.
      foreach($client->options['curl_options'] as $key => $value){
        $curlopt[$key] = $value;
      }
    }
    curl_setopt_array($client->handle, $curlopt);

    $client->parse_response(curl_exec($client->handle));
    $client->info = (object) curl_getinfo($client->handle);
    $client->error = curl_error($client->handle);

    curl_close($client->handle);
    return $client;
  }

  public function format_query($parameters, $primary='=', $secondary='&'){
    $query = "";
    foreach($parameters as $key => $value){
      $pair = array(urlencode($key), urlencode($value));
      $query .= implode($primary, $pair) . $secondary;
    }
    return rtrim($query, $secondary);
  }

  public function parse_response($response){
    $headers = array();
    $http_ver = strtok($response, "\n");

    while($line = strtok("\n")){
      if(strlen(trim($line)) == 0) break;

      list($key, $value) = explode(':', $line, 2);
      $key = trim(strtolower(str_replace('-', '_', $key)));
      $value = trim($value);
      if(empty($headers[$key]))
        $headers[$key] = $value;
      elseif(is_array($headers[$key]))
        $headers[$key][] = $value;
      else
        $headers[$key] = array($headers[$key], $value);
    }

    $this->headers = (object) $headers;
    $this->response = strtok("");
  }

  public function get_response_format(){
    if(!$this->response)
      throw new RestClientException(
        "A response must exist before it can be decoded.");

    // User-defined format.
    if(!empty($this->options['format']))
      return $this->options['format'];

    // Extract format from response content-type header.
    if(!empty($this->headers->content_type))
      if(preg_match($this->options['format_regex'], $this->headers->content_type, $matches))
        return $matches[2];

    throw new RestClientException(
      "Response format could not be determined.");
  }

  public function decode_response(){
    if(empty($this->decoded_response)){
      $format = $this->get_response_format();
      if(!array_key_exists($format, $this->options['decoders']))
        throw new RestClientException("'${format}' is not a supported ".
          "format, register a decoder to handle this response.");

      $this->decoded_response = call_user_func(
        $this->options['decoders'][$format], $this->response);
    }

    return $this->decoded_response;
  }
}


/**
 * Application starts here
 */
use Phalcon\Mvc\Micro;

$app = new Micro();

$app->get('/', function () {
  $api = new RestClient(array(
    'base_url' => "http://iivakic.riteh.hexis.hr/",
  ));

  $result = $api->get('/');

  echo '<pre>'; var_dump($result->response); echo '</pre>';
});

$app->get('/get-signature', function () {

  $api = new RestClient(array(
    'base_url' => "http://iivakic.riteh.hexis.hr/",
  ));

  $result = $api->post("get-signature", array("username"=>"jurada"));

  echo $result->response;

});

$app->get('/get-questions', function () {
  $api = new RestClient(array(
    'base_url' => "http://iivakic.riteh.hexis.hr/",
  ));

  $result = $api->post("get-questions", array("username"=>"jurada", 'signature' => 'riteh_jurada_56548c70a4452'));

  echo $result->response;
});

$app->get('/send-answers', function () {
  $api = new RestClient(array(
    'base_url' => "http://iivakic.riteh.hexis.hr/",
  ));

  $result = $api->post("answer", array(
    "username"=>"jurada",
    'signature' => 'riteh_jurada_56548c70a4452',
    'odgovori' => json_encode(array(
      array(
        'id' => 9,
        'odgovor' => 'Razliku mo�emo vidjeti tako da prvo fetchamo taj udaljeni file koji �elimo, �to ne�e primjeniti promjene, ve� samo spremiti index promjena, te onda napravimo -diff udaljeni_branch nas-branch-. To �e nam dati razliku izme�u ta 2 filea.'
      ),
	  array(
        'id' => 10,
        'odgovor' => 'DNS je domain name server, tojest server koji povezuje IP adrese sa domain imenima, npr da mi napi�emo google.hr, ta domena �e preko dns-a �e nas preusmjeriti na odre�enu ip adresu na neki od googlovih servera. Funkciju lokalnog dns-a obavlja hosts datoteka koja istu funkciju, samo na manjoj skali.'
      ),
	  array(
        'id' => 11,
        'odgovor' => 'Hosts datoteka slu�i kao lokalni mali dns server. povezuje odre�ene IP adrese sa odre�enim imenima, -browser adresama-. Primjer unosa je -192.168.4.4 moj.virtualni.server nginxserv-, gdje je IP adresa na koju se preusmjeruje, moj.virt... naziv koji �e se preusmjeravati na ip, te sve kasnije su aliasi.'
      ),
	  array(
        'id' => 13,
        'odgovor' => 'Apache i Nginx su web server programi koji koriste http ili https protokole za komunikaciju i manipulaciju akcijama i resursima. Zaprimaju zahtjeve i �alju odgovore na te zahtjeve. Apache se �e��e koristi u manjim i manje kori�tenim aplikacijama i od svakog zahtjeva radi novi thread, dok nginx koristi workere, i �tedi vi�e ram-a.'
      ),
	  array(
        'id' => 15,
        'odgovor' => 'HTTP je hypertext transfer protocol, radi uglavnom na portu 80, te definira http glagole - GET,POST,HEAD,DELETE,PUT itd, od kojih bi svaki trebao imat odre�enu funkciju, iako njihovo zna�enje je samo semanti�ko. mi ako zaprimimo POST zahtjev, na serveru mo�emo obaviti �to god ho�emo i isprogramiramo.'
      ),
	  array(
        'id' => 16,
        'odgovor' => 'HTTPS je sigurna verzija http-a, radi na portu 443, te koristi SSL. HTTPS veze su enkriptirane i tako su sigurne od packet sniffinga i raznih man-in-the-middle napada. Uglavnom se najvi�e koristi pri loginovima i sli�no gdje se prenose osjetljivi podaci.'
      ),
	  array(
        'id' => 17,
        'odgovor' => 'Neki od http glagola su GET,POST,HEAD,DELETE,PUT, od kojih svaki ima odre�enu funkciju za koju bi se TREBAO koristiti, iako njihovo zna�enje je samo semanti�ko. mi ako zaprimimo POST zahtjev, na serveru mo�emo obaviti �to god ho�emo i isprogramiramo.'
      ),
	  array(
        'id' => 34,
        'odgovor' => 'Framework je slojeviti sustav koji omogu�ava razvijanje aplikacija. Uglavnom ve� ima implementiran veliki broj funkcija koje olak�avaju i strukturiraju rad. Potrebno je puno manje koda nego kad bi istu aplikaciju htjeli implementirati �istim programskim jezikom, npr PHP. Neki od frameworka za php koj smo vidjeli na ovom kolegiju su phalcon, zend i yii ...'
      )
      )
	  )
    )
  );

  var_dump($result->response);
});

$app->notFound(function () use ($app) {
  $app->response->setStatusCode(404, "Not Found")->sendHeaders();
  echo 'Malo ste zalutali!';
});

$app->handle();



